// Generated by gir (https://github.com/gtk-rs/gir @ eb5be4f1bafe)
// from ../../ev-girs (@ 7c3a00d0bdc2+)
// from ../../gir-files (@ 20031a537e40)
// DO NOT EDIT

#![allow(non_camel_case_types, non_upper_case_globals, non_snake_case)]
#![allow(
    clippy::approx_constant,
    clippy::type_complexity,
    clippy::unreadable_literal,
    clippy::upper_case_acronyms
)]
#![cfg_attr(docsrs, feature(doc_cfg))]

use cairo_sys as cairo;
use gdk_sys as gdk;
use gio_sys as gio;
use glib_sys as glib;
use gobject_sys as gobject;
use gtk_sys as gtk;
use papers_document_sys as papers_document;

#[allow(unused_imports)]
use libc::{
    c_char, c_double, c_float, c_int, c_long, c_short, c_uchar, c_uint, c_ulong, c_ushort, c_void,
    intptr_t, off_t, size_t, ssize_t, time_t, uintptr_t, FILE,
};
#[cfg(unix)]
#[allow(unused_imports)]
use libc::{dev_t, gid_t, pid_t, socklen_t, uid_t};

#[allow(unused_imports)]
use glib::{gboolean, gconstpointer, gpointer, GType};

// Enums
pub type PpsJobPriority = c_int;
pub const PPS_JOB_PRIORITY_URGENT: PpsJobPriority = 0;
pub const PPS_JOB_PRIORITY_HIGH: PpsJobPriority = 1;
pub const PPS_JOB_PRIORITY_LOW: PpsJobPriority = 2;
pub const PPS_JOB_PRIORITY_NONE: PpsJobPriority = 3;
pub const PPS_JOB_N_PRIORITIES: PpsJobPriority = 4;

pub type PpsPageLayout = c_int;
pub const PPS_PAGE_LAYOUT_SINGLE: PpsPageLayout = 0;
pub const PPS_PAGE_LAYOUT_DUAL: PpsPageLayout = 1;
pub const PPS_PAGE_LAYOUT_AUTOMATIC: PpsPageLayout = 2;

pub type PpsSizingMode = c_int;
pub const PPS_SIZING_FIT_PAGE: PpsSizingMode = 0;
pub const PPS_SIZING_FIT_WIDTH: PpsSizingMode = 1;
pub const PPS_SIZING_FREE: PpsSizingMode = 2;
pub const PPS_SIZING_AUTOMATIC: PpsSizingMode = 3;

// Flags
pub type PpsJobPageDataFlags = c_uint;
pub const PPS_PAGE_DATA_INCLUDE_NONE: PpsJobPageDataFlags = 0;
pub const PPS_PAGE_DATA_INCLUDE_LINKS: PpsJobPageDataFlags = 1;
pub const PPS_PAGE_DATA_INCLUDE_TEXT: PpsJobPageDataFlags = 2;
pub const PPS_PAGE_DATA_INCLUDE_TEXT_MAPPING: PpsJobPageDataFlags = 4;
pub const PPS_PAGE_DATA_INCLUDE_TEXT_LAYOUT: PpsJobPageDataFlags = 8;
pub const PPS_PAGE_DATA_INCLUDE_TEXT_ATTRS: PpsJobPageDataFlags = 16;
pub const PPS_PAGE_DATA_INCLUDE_TEXT_LOG_ATTRS: PpsJobPageDataFlags = 32;
pub const PPS_PAGE_DATA_INCLUDE_IMAGES: PpsJobPageDataFlags = 64;
pub const PPS_PAGE_DATA_INCLUDE_FORMS: PpsJobPageDataFlags = 128;
pub const PPS_PAGE_DATA_INCLUDE_ANNOTS: PpsJobPageDataFlags = 256;
pub const PPS_PAGE_DATA_INCLUDE_MEDIA: PpsJobPageDataFlags = 512;
pub const PPS_PAGE_DATA_INCLUDE_ALL: PpsJobPageDataFlags = 1023;

// Records
#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsDocumentModelClass {
    pub parent_class: gobject::GObjectClass,
}

impl ::std::fmt::Debug for PpsDocumentModelClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsDocumentModelClass @ {self:p}"))
            .field("parent_class", &self.parent_class)
            .finish()
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsJobAnnotsClass {
    pub parent_class: PpsJobClass,
}

impl ::std::fmt::Debug for PpsJobAnnotsClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsJobAnnotsClass @ {self:p}"))
            .field("parent_class", &self.parent_class)
            .finish()
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsJobAttachmentsClass {
    pub parent_class: PpsJobClass,
}

impl ::std::fmt::Debug for PpsJobAttachmentsClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsJobAttachmentsClass @ {self:p}"))
            .field("parent_class", &self.parent_class)
            .finish()
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsJobClass {
    pub parent_class: gobject::GObjectClass,
    pub run: Option<unsafe extern "C" fn(*mut PpsJob) -> gboolean>,
    pub cancelled: Option<unsafe extern "C" fn(*mut PpsJob)>,
    pub finished: Option<unsafe extern "C" fn(*mut PpsJob)>,
}

impl ::std::fmt::Debug for PpsJobClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsJobClass @ {self:p}"))
            .field("parent_class", &self.parent_class)
            .field("run", &self.run)
            .field("cancelled", &self.cancelled)
            .field("finished", &self.finished)
            .finish()
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsJobExportClass {
    pub parent_class: PpsJobClass,
}

impl ::std::fmt::Debug for PpsJobExportClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsJobExportClass @ {self:p}"))
            .field("parent_class", &self.parent_class)
            .finish()
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsJobFindClass {
    pub parent_class: PpsJobClass,
    pub updated: Option<unsafe extern "C" fn(*mut PpsJobFind, c_int)>,
}

impl ::std::fmt::Debug for PpsJobFindClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsJobFindClass @ {self:p}"))
            .field("parent_class", &self.parent_class)
            .field("updated", &self.updated)
            .finish()
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsJobFontsClass {
    pub parent_class: PpsJobClass,
}

impl ::std::fmt::Debug for PpsJobFontsClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsJobFontsClass @ {self:p}"))
            .field("parent_class", &self.parent_class)
            .finish()
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsJobLayersClass {
    pub parent_class: PpsJobClass,
}

impl ::std::fmt::Debug for PpsJobLayersClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsJobLayersClass @ {self:p}"))
            .field("parent_class", &self.parent_class)
            .finish()
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsJobLinksClass {
    pub parent_class: PpsJobClass,
}

impl ::std::fmt::Debug for PpsJobLinksClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsJobLinksClass @ {self:p}"))
            .field("parent_class", &self.parent_class)
            .finish()
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsJobLoadClass {
    pub parent_class: PpsJobClass,
}

impl ::std::fmt::Debug for PpsJobLoadClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsJobLoadClass @ {self:p}"))
            .field("parent_class", &self.parent_class)
            .finish()
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsJobPageDataClass {
    pub parent_class: PpsJobClass,
}

impl ::std::fmt::Debug for PpsJobPageDataClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsJobPageDataClass @ {self:p}"))
            .field("parent_class", &self.parent_class)
            .finish()
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsJobPrintClass {
    pub parent_class: PpsJobClass,
}

impl ::std::fmt::Debug for PpsJobPrintClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsJobPrintClass @ {self:p}"))
            .field("parent_class", &self.parent_class)
            .finish()
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsJobRenderTextureClass {
    pub parent_class: PpsJobClass,
}

impl ::std::fmt::Debug for PpsJobRenderTextureClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsJobRenderTextureClass @ {self:p}"))
            .field("parent_class", &self.parent_class)
            .finish()
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsJobSaveClass {
    pub parent_class: PpsJobClass,
}

impl ::std::fmt::Debug for PpsJobSaveClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsJobSaveClass @ {self:p}"))
            .field("parent_class", &self.parent_class)
            .finish()
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsJobThumbnailTextureClass {
    pub parent_class: PpsJobClass,
}

impl ::std::fmt::Debug for PpsJobThumbnailTextureClass {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsJobThumbnailTextureClass @ {self:p}"))
            .field("parent_class", &self.parent_class)
            .finish()
    }
}

#[repr(C)]
pub struct _PpsPrintOperationClass {
    _data: [u8; 0],
    _marker: core::marker::PhantomData<(*mut u8, core::marker::PhantomPinned)>,
}

pub type PpsPrintOperationClass = _PpsPrintOperationClass;

#[repr(C)]
pub struct _PpsViewClass {
    _data: [u8; 0],
    _marker: core::marker::PhantomData<(*mut u8, core::marker::PhantomPinned)>,
}

pub type PpsViewClass = _PpsViewClass;

#[repr(C)]
pub struct _PpsViewPresentationClass {
    _data: [u8; 0],
    _marker: core::marker::PhantomData<(*mut u8, core::marker::PhantomPinned)>,
}

pub type PpsViewPresentationClass = _PpsViewPresentationClass;

// Classes
#[repr(C)]
pub struct PpsDocumentModel {
    _data: [u8; 0],
    _marker: core::marker::PhantomData<(*mut u8, core::marker::PhantomPinned)>,
}

impl ::std::fmt::Debug for PpsDocumentModel {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsDocumentModel @ {self:p}"))
            .finish()
    }
}

#[repr(C)]
pub struct PpsJob {
    pub parent: gobject::GObject,
    pub document: *mut papers_document::PpsDocument,
    pub cancelled: c_uint,
    _truncated_record_marker: c_void,
    // field finished has incomplete type
}

impl ::std::fmt::Debug for PpsJob {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsJob @ {self:p}"))
            .field("parent", &self.parent)
            .field("document", &self.document)
            .field("cancelled", &self.cancelled)
            .finish()
    }
}

#[repr(C)]
pub struct PpsJobAnnots {
    _truncated_record_marker: c_void,
    // /*Ignored*/field parent has incomplete type
}

impl ::std::fmt::Debug for PpsJobAnnots {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsJobAnnots @ {self:p}")).finish()
    }
}

#[repr(C)]
pub struct PpsJobAttachments {
    _truncated_record_marker: c_void,
    // /*Ignored*/field parent has incomplete type
}

impl ::std::fmt::Debug for PpsJobAttachments {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsJobAttachments @ {self:p}"))
            .finish()
    }
}

#[repr(C)]
pub struct PpsJobExport {
    _truncated_record_marker: c_void,
    // /*Ignored*/field parent has incomplete type
}

impl ::std::fmt::Debug for PpsJobExport {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsJobExport @ {self:p}")).finish()
    }
}

#[repr(C)]
pub struct PpsJobFind {
    _truncated_record_marker: c_void,
    // /*Ignored*/field parent has incomplete type
}

impl ::std::fmt::Debug for PpsJobFind {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsJobFind @ {self:p}")).finish()
    }
}

#[repr(C)]
pub struct PpsJobFonts {
    _truncated_record_marker: c_void,
    // /*Ignored*/field parent has incomplete type
}

impl ::std::fmt::Debug for PpsJobFonts {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsJobFonts @ {self:p}")).finish()
    }
}

#[repr(C)]
pub struct PpsJobLayers {
    _truncated_record_marker: c_void,
    // /*Ignored*/field parent has incomplete type
}

impl ::std::fmt::Debug for PpsJobLayers {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsJobLayers @ {self:p}")).finish()
    }
}

#[repr(C)]
pub struct PpsJobLinks {
    _truncated_record_marker: c_void,
    // /*Ignored*/field parent has incomplete type
}

impl ::std::fmt::Debug for PpsJobLinks {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsJobLinks @ {self:p}")).finish()
    }
}

#[repr(C)]
pub struct PpsJobLoad {
    _truncated_record_marker: c_void,
    // /*Ignored*/field parent has incomplete type
}

impl ::std::fmt::Debug for PpsJobLoad {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsJobLoad @ {self:p}")).finish()
    }
}

#[repr(C)]
pub struct PpsJobPageData {
    _truncated_record_marker: c_void,
    // /*Ignored*/field parent has incomplete type
}

impl ::std::fmt::Debug for PpsJobPageData {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsJobPageData @ {self:p}"))
            .finish()
    }
}

#[repr(C)]
pub struct PpsJobPrint {
    _truncated_record_marker: c_void,
    // /*Ignored*/field parent has incomplete type
}

impl ::std::fmt::Debug for PpsJobPrint {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsJobPrint @ {self:p}")).finish()
    }
}

#[repr(C)]
pub struct PpsJobRenderTexture {
    _truncated_record_marker: c_void,
    // /*Ignored*/field parent has incomplete type
}

impl ::std::fmt::Debug for PpsJobRenderTexture {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsJobRenderTexture @ {self:p}"))
            .finish()
    }
}

#[repr(C)]
pub struct PpsJobSave {
    _truncated_record_marker: c_void,
    // /*Ignored*/field parent has incomplete type
}

impl ::std::fmt::Debug for PpsJobSave {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsJobSave @ {self:p}")).finish()
    }
}

#[repr(C)]
pub struct PpsJobThumbnailTexture {
    _truncated_record_marker: c_void,
    // /*Ignored*/field parent has incomplete type
}

impl ::std::fmt::Debug for PpsJobThumbnailTexture {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsJobThumbnailTexture @ {self:p}"))
            .finish()
    }
}

#[repr(C)]
pub struct PpsPrintOperation {
    _data: [u8; 0],
    _marker: core::marker::PhantomData<(*mut u8, core::marker::PhantomPinned)>,
}

impl ::std::fmt::Debug for PpsPrintOperation {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsPrintOperation @ {self:p}"))
            .finish()
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsView {
    pub parent_instance: gtk::GtkWidget,
}

impl ::std::fmt::Debug for PpsView {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsView @ {self:p}"))
            .field("parent_instance", &self.parent_instance)
            .finish()
    }
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct PpsViewPresentation {
    pub parent_instance: gtk::GtkWidget,
}

impl ::std::fmt::Debug for PpsViewPresentation {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        f.debug_struct(&format!("PpsViewPresentation @ {self:p}"))
            .field("parent_instance", &self.parent_instance)
            .finish()
    }
}

#[link(name = "ppsview-4.0")]
#[link(name = "ppsdocument-4.0")]
extern "C" {

    //=========================================================================
    // PpsJobPriority
    //=========================================================================
    pub fn pps_job_priority_get_type() -> GType;

    //=========================================================================
    // PpsPageLayout
    //=========================================================================
    pub fn pps_page_layout_get_type() -> GType;

    //=========================================================================
    // PpsSizingMode
    //=========================================================================
    pub fn pps_sizing_mode_get_type() -> GType;

    //=========================================================================
    // PpsJobPageDataFlags
    //=========================================================================
    pub fn pps_job_page_data_flags_get_type() -> GType;

    //=========================================================================
    // PpsDocumentModel
    //=========================================================================
    pub fn pps_document_model_get_type() -> GType;
    pub fn pps_document_model_new() -> *mut PpsDocumentModel;
    pub fn pps_document_model_new_with_document(
        document: *mut papers_document::PpsDocument,
    ) -> *mut PpsDocumentModel;
    pub fn pps_document_model_get_continuous(model: *mut PpsDocumentModel) -> gboolean;
    pub fn pps_document_model_get_document(
        model: *mut PpsDocumentModel,
    ) -> *mut papers_document::PpsDocument;
    pub fn pps_document_model_get_dual_page_odd_pages_left(
        model: *mut PpsDocumentModel,
    ) -> gboolean;
    pub fn pps_document_model_get_inverted_colors(model: *mut PpsDocumentModel) -> gboolean;
    pub fn pps_document_model_get_max_scale(model: *mut PpsDocumentModel) -> c_double;
    pub fn pps_document_model_get_min_scale(model: *mut PpsDocumentModel) -> c_double;
    pub fn pps_document_model_get_page(model: *mut PpsDocumentModel) -> c_int;
    pub fn pps_document_model_get_page_layout(model: *mut PpsDocumentModel) -> PpsPageLayout;
    pub fn pps_document_model_get_rotation(model: *mut PpsDocumentModel) -> c_int;
    pub fn pps_document_model_get_rtl(model: *mut PpsDocumentModel) -> gboolean;
    pub fn pps_document_model_get_scale(model: *mut PpsDocumentModel) -> c_double;
    pub fn pps_document_model_get_sizing_mode(model: *mut PpsDocumentModel) -> PpsSizingMode;
    pub fn pps_document_model_set_continuous(model: *mut PpsDocumentModel, continuous: gboolean);
    pub fn pps_document_model_set_document(
        model: *mut PpsDocumentModel,
        document: *mut papers_document::PpsDocument,
    );
    pub fn pps_document_model_set_dual_page_odd_pages_left(
        model: *mut PpsDocumentModel,
        odd_left: gboolean,
    );
    pub fn pps_document_model_set_inverted_colors(
        model: *mut PpsDocumentModel,
        inverted_colors: gboolean,
    );
    pub fn pps_document_model_set_max_scale(model: *mut PpsDocumentModel, max_scale: c_double);
    pub fn pps_document_model_set_min_scale(model: *mut PpsDocumentModel, min_scale: c_double);
    pub fn pps_document_model_set_page(model: *mut PpsDocumentModel, page: c_int);
    pub fn pps_document_model_set_page_by_label(
        model: *mut PpsDocumentModel,
        page_label: *const c_char,
    );
    pub fn pps_document_model_set_page_layout(model: *mut PpsDocumentModel, layout: PpsPageLayout);
    pub fn pps_document_model_set_rotation(model: *mut PpsDocumentModel, rotation: c_int);
    pub fn pps_document_model_set_rtl(model: *mut PpsDocumentModel, rtl: gboolean);
    pub fn pps_document_model_set_scale(model: *mut PpsDocumentModel, scale: c_double);
    pub fn pps_document_model_set_sizing_mode(model: *mut PpsDocumentModel, mode: PpsSizingMode);

    //=========================================================================
    // PpsJob
    //=========================================================================
    pub fn pps_job_get_type() -> GType;
    pub fn pps_job_scheduler_wait();
    pub fn pps_job_cancel(job: *mut PpsJob);
    pub fn pps_job_failed(
        job: *mut PpsJob,
        domain: glib::GQuark,
        code: c_int,
        format: *const c_char,
        ...
    );
    pub fn pps_job_failed_from_error(job: *mut PpsJob, error: *mut glib::GError);
    pub fn pps_job_get_document(job: *mut PpsJob) -> *mut papers_document::PpsDocument;
    pub fn pps_job_is_finished(job: *mut PpsJob) -> gboolean;
    pub fn pps_job_is_succeeded(job: *mut PpsJob, error: *mut *mut glib::GError) -> gboolean;
    pub fn pps_job_run(job: *mut PpsJob) -> gboolean;
    pub fn pps_job_scheduler_push_job(job: *mut PpsJob, priority: PpsJobPriority);
    pub fn pps_job_scheduler_update_job(job: *mut PpsJob, priority: PpsJobPriority);
    pub fn pps_job_succeeded(job: *mut PpsJob);

    //=========================================================================
    // PpsJobAnnots
    //=========================================================================
    pub fn pps_job_annots_get_type() -> GType;
    pub fn pps_job_annots_new(document: *mut papers_document::PpsDocument) -> *mut PpsJob;

    //=========================================================================
    // PpsJobAttachments
    //=========================================================================
    pub fn pps_job_attachments_get_type() -> GType;
    pub fn pps_job_attachments_new(document: *mut papers_document::PpsDocument) -> *mut PpsJob;
    pub fn pps_job_attachments_get_attachments(
        job_attachments: *mut PpsJobAttachments,
    ) -> *mut glib::GList;

    //=========================================================================
    // PpsJobExport
    //=========================================================================
    pub fn pps_job_export_get_type() -> GType;
    pub fn pps_job_export_new(document: *mut papers_document::PpsDocument) -> *mut PpsJob;
    pub fn pps_job_export_set_page(job: *mut PpsJobExport, page: c_int);

    //=========================================================================
    // PpsJobFind
    //=========================================================================
    pub fn pps_job_find_get_type() -> GType;
    pub fn pps_job_find_new(
        document: *mut papers_document::PpsDocument,
        start_page: c_int,
        n_pages: c_int,
        text: *const c_char,
        options: papers_document::PpsFindOptions,
    ) -> *mut PpsJob;
    pub fn pps_job_find_get_n_main_results(job: *mut PpsJobFind, page: c_int) -> c_int;
    pub fn pps_job_find_get_options(job: *mut PpsJobFind) -> papers_document::PpsFindOptions;
    pub fn pps_job_find_get_results(job: *mut PpsJobFind) -> *mut *mut glib::GList;
    pub fn pps_job_find_has_results(job: *mut PpsJobFind) -> gboolean;

    //=========================================================================
    // PpsJobFonts
    //=========================================================================
    pub fn pps_job_fonts_get_type() -> GType;
    pub fn pps_job_fonts_new(document: *mut papers_document::PpsDocument) -> *mut PpsJob;

    //=========================================================================
    // PpsJobLayers
    //=========================================================================
    pub fn pps_job_layers_get_type() -> GType;
    pub fn pps_job_layers_new(document: *mut papers_document::PpsDocument) -> *mut PpsJob;
    pub fn pps_job_layers_get_model(job_layers: *mut PpsJobLayers) -> *mut gio::GListModel;

    //=========================================================================
    // PpsJobLinks
    //=========================================================================
    pub fn pps_job_links_get_type() -> GType;
    pub fn pps_job_links_new(document: *mut papers_document::PpsDocument) -> *mut PpsJob;
    pub fn pps_job_links_get_model(job: *mut PpsJobLinks) -> *mut gio::GListModel;

    //=========================================================================
    // PpsJobLoad
    //=========================================================================
    pub fn pps_job_load_get_type() -> GType;
    pub fn pps_job_load_new() -> *mut PpsJob;
    #[cfg(feature = "v46")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v46")))]
    pub fn pps_job_load_get_loaded_document(
        job: *mut PpsJobLoad,
    ) -> *mut papers_document::PpsDocument;
    #[cfg(feature = "v46")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v46")))]
    pub fn pps_job_load_set_fd(
        job: *mut PpsJobLoad,
        fd: c_int,
        mime_type: *const c_char,
        error: *mut *mut glib::GError,
    ) -> gboolean;
    pub fn pps_job_load_set_load_flags(
        job: *mut PpsJobLoad,
        flags: papers_document::PpsDocumentLoadFlags,
    );
    pub fn pps_job_load_set_password(job: *mut PpsJobLoad, password: *const c_char);
    pub fn pps_job_load_set_uri(job: *mut PpsJobLoad, uri: *const c_char);
    #[cfg(feature = "v46")]
    #[cfg_attr(docsrs, doc(cfg(feature = "v46")))]
    pub fn pps_job_load_take_fd(job: *mut PpsJobLoad, fd: c_int, mime_type: *const c_char);

    //=========================================================================
    // PpsJobPageData
    //=========================================================================
    pub fn pps_job_page_data_get_type() -> GType;
    pub fn pps_job_page_data_new(
        document: *mut papers_document::PpsDocument,
        page: c_int,
        flags: PpsJobPageDataFlags,
    ) -> *mut PpsJob;

    //=========================================================================
    // PpsJobPrint
    //=========================================================================
    pub fn pps_job_print_get_type() -> GType;
    pub fn pps_job_print_new(document: *mut papers_document::PpsDocument) -> *mut PpsJob;
    pub fn pps_job_print_set_cairo(job: *mut PpsJobPrint, cr: *mut cairo::cairo_t);
    pub fn pps_job_print_set_page(job: *mut PpsJobPrint, page: c_int);

    //=========================================================================
    // PpsJobRenderTexture
    //=========================================================================
    pub fn pps_job_render_texture_get_type() -> GType;
    pub fn pps_job_render_texture_new(
        document: *mut papers_document::PpsDocument,
        page: c_int,
        rotation: c_int,
        scale: c_double,
        width: c_int,
        height: c_int,
    ) -> *mut PpsJob;
    pub fn pps_job_render_texture_set_selection_info(
        job: *mut PpsJobRenderTexture,
        selection_points: *mut papers_document::PpsRectangle,
        selection_style: papers_document::PpsSelectionStyle,
        text: *mut gdk::GdkRGBA,
        base: *mut gdk::GdkRGBA,
    );

    //=========================================================================
    // PpsJobSave
    //=========================================================================
    pub fn pps_job_save_get_type() -> GType;
    pub fn pps_job_save_new(
        document: *mut papers_document::PpsDocument,
        uri: *const c_char,
        document_uri: *const c_char,
    ) -> *mut PpsJob;

    //=========================================================================
    // PpsJobThumbnailTexture
    //=========================================================================
    pub fn pps_job_thumbnail_texture_get_type() -> GType;
    pub fn pps_job_thumbnail_texture_new(
        document: *mut papers_document::PpsDocument,
        page: c_int,
        rotation: c_int,
        scale: c_double,
    ) -> *mut PpsJob;
    pub fn pps_job_thumbnail_texture_new_with_target_size(
        document: *mut papers_document::PpsDocument,
        page: c_int,
        rotation: c_int,
        target_width: c_int,
        target_height: c_int,
    ) -> *mut PpsJob;
    pub fn pps_job_thumbnail_texture_get_texture(
        job: *mut PpsJobThumbnailTexture,
    ) -> *mut gdk::GdkTexture;

    //=========================================================================
    // PpsPrintOperation
    //=========================================================================
    pub fn pps_print_operation_get_type() -> GType;
    pub fn pps_print_operation_new(
        document: *mut papers_document::PpsDocument,
    ) -> *mut PpsPrintOperation;
    pub fn pps_print_operation_exists_for_document(
        document: *mut papers_document::PpsDocument,
    ) -> gboolean;
    pub fn pps_print_operation_cancel(op: *mut PpsPrintOperation);
    pub fn pps_print_operation_get_default_page_setup(
        op: *mut PpsPrintOperation,
    ) -> *mut gtk::GtkPageSetup;
    pub fn pps_print_operation_get_embed_page_setup(op: *mut PpsPrintOperation) -> gboolean;
    pub fn pps_print_operation_get_error(op: *mut PpsPrintOperation, error: *mut *mut glib::GError);
    pub fn pps_print_operation_get_job_name(op: *mut PpsPrintOperation) -> *const c_char;
    pub fn pps_print_operation_get_print_settings(
        op: *mut PpsPrintOperation,
    ) -> *mut gtk::GtkPrintSettings;
    pub fn pps_print_operation_get_progress(op: *mut PpsPrintOperation) -> c_double;
    pub fn pps_print_operation_get_status(op: *mut PpsPrintOperation) -> *const c_char;
    pub fn pps_print_operation_run(op: *mut PpsPrintOperation, parent: *mut gtk::GtkWindow);
    pub fn pps_print_operation_set_current_page(op: *mut PpsPrintOperation, current_page: c_int);
    pub fn pps_print_operation_set_default_page_setup(
        op: *mut PpsPrintOperation,
        page_setup: *mut gtk::GtkPageSetup,
    );
    pub fn pps_print_operation_set_embed_page_setup(op: *mut PpsPrintOperation, embed: gboolean);
    pub fn pps_print_operation_set_job_name(op: *mut PpsPrintOperation, job_name: *const c_char);
    pub fn pps_print_operation_set_print_settings(
        op: *mut PpsPrintOperation,
        print_settings: *mut gtk::GtkPrintSettings,
    );

    //=========================================================================
    // PpsView
    //=========================================================================
    pub fn pps_view_get_type() -> GType;
    pub fn pps_view_new() -> *mut PpsView;
    pub fn pps_view_get_resource() -> *mut gio::GResource;
    pub fn pps_view_add_text_markup_annotation_for_selected_text(view: *mut PpsView) -> gboolean;
    pub fn pps_view_begin_add_text_annotation(view: *mut PpsView);
    pub fn pps_view_can_zoom_in(view: *mut PpsView) -> gboolean;
    pub fn pps_view_can_zoom_out(view: *mut PpsView) -> gboolean;
    pub fn pps_view_cancel_add_text_annotation(view: *mut PpsView);
    pub fn pps_view_copy(view: *mut PpsView);
    pub fn pps_view_copy_link_address(
        view: *mut PpsView,
        action: *mut papers_document::PpsLinkAction,
    );
    pub fn pps_view_current_event_is_type(view: *mut PpsView, type_: gdk::GdkEventType)
        -> gboolean;
    pub fn pps_view_find_cancel(view: *mut PpsView);
    pub fn pps_view_find_next(view: *mut PpsView);
    pub fn pps_view_find_previous(view: *mut PpsView);
    pub fn pps_view_find_restart(view: *mut PpsView, page: c_int);
    pub fn pps_view_find_set_highlight_search(view: *mut PpsView, value: gboolean);
    pub fn pps_view_find_set_result(view: *mut PpsView, page: c_int, result: c_int);
    pub fn pps_view_find_started(view: *mut PpsView, job: *mut PpsJobFind);
    pub fn pps_view_focus_annotation(
        view: *mut PpsView,
        annot_mapping: *mut papers_document::PpsMapping,
    );
    pub fn pps_view_get_allow_links_change_zoom(view: *mut PpsView) -> gboolean;
    pub fn pps_view_get_enable_spellchecking(view: *mut PpsView) -> gboolean;
    pub fn pps_view_get_page_extents(
        view: *mut PpsView,
        page: c_int,
        page_area: *mut gdk::GdkRectangle,
        border: *mut gtk::GtkBorder,
    ) -> gboolean;
    pub fn pps_view_get_page_extents_for_border(
        view: *mut PpsView,
        page: c_int,
        border: *mut gtk::GtkBorder,
        page_area: *mut gdk::GdkRectangle,
    ) -> gboolean;
    pub fn pps_view_get_selected_text(view: *mut PpsView) -> *mut c_char;
    pub fn pps_view_handle_link(view: *mut PpsView, link: *mut papers_document::PpsLink);
    pub fn pps_view_has_selection(view: *mut PpsView) -> gboolean;
    pub fn pps_view_is_caret_navigation_enabled(view: *mut PpsView) -> gboolean;
    pub fn pps_view_is_loading(view: *mut PpsView) -> gboolean;
    pub fn pps_view_next_page(view: *mut PpsView) -> gboolean;
    pub fn pps_view_previous_page(view: *mut PpsView) -> gboolean;
    pub fn pps_view_reload(view: *mut PpsView);
    pub fn pps_view_remove_annotation(
        view: *mut PpsView,
        annot: *mut papers_document::PpsAnnotation,
    );
    pub fn pps_view_select_all(view: *mut PpsView);
    pub fn pps_view_set_allow_links_change_zoom(view: *mut PpsView, allowed: gboolean);
    pub fn pps_view_set_caret_cursor_position(view: *mut PpsView, page: c_uint, offset: c_uint);
    pub fn pps_view_set_caret_navigation_enabled(view: *mut PpsView, enabled: gboolean);
    pub fn pps_view_set_enable_spellchecking(view: *mut PpsView, spellcheck: gboolean);
    pub fn pps_view_set_model(view: *mut PpsView, model: *mut PpsDocumentModel);
    pub fn pps_view_set_page_cache_size(view: *mut PpsView, cache_size: size_t);
    pub fn pps_view_supports_caret_navigation(view: *mut PpsView) -> gboolean;
    pub fn pps_view_zoom_in(view: *mut PpsView);
    pub fn pps_view_zoom_out(view: *mut PpsView);

    //=========================================================================
    // PpsViewPresentation
    //=========================================================================
    pub fn pps_view_presentation_get_type() -> GType;
    pub fn pps_view_presentation_new(
        document: *mut papers_document::PpsDocument,
        current_page: c_uint,
        rotation: c_uint,
        inverted_colors: gboolean,
    ) -> *mut PpsViewPresentation;
    pub fn pps_view_presentation_get_current_page(pview: *mut PpsViewPresentation) -> c_uint;
    pub fn pps_view_presentation_get_rotation(pview: *mut PpsViewPresentation) -> c_uint;
    pub fn pps_view_presentation_next_page(pview: *mut PpsViewPresentation);
    pub fn pps_view_presentation_previous_page(pview: *mut PpsViewPresentation);
    pub fn pps_view_presentation_set_rotation(pview: *mut PpsViewPresentation, rotation: c_int);

}
